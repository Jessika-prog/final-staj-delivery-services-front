import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Commande } from '../shared/models/commande.model';
import { CommandeWebService } from '../shared/web-services/commande.webservice';



@Component({
  selector: 'app-afficher-commandes',
  templateUrl: './afficher-commandes.component.html',
  styleUrls: ['./afficher-commandes.component.scss']
})
export class AfficherCommandesComponent implements OnInit, OnDestroy {

  private subscriptionMessage$: Subscription;

  constructor(private commandeWebService: CommandeWebService) { }


  commandeList: Commande[];
  isComplete = false;

  ngOnInit() {
    this.subscriptionMessage$ = this.commandeWebService.getCommandes().subscribe(
      (data) => {
        // Next
        console.log('CallObservableComponent Next commande' );
        this.commandeList = data;
      },(error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }, () => {
        // Complete
        console.log('CallObservableComponent Complete commande');
        this.isComplete = true;
      }
    );
    // this.subscriptionMessage$ = this.clientWebService.getClients().subscribe(
    //   (dataClient) => {
    //     // Next
    //     console.log('CallObservableComponent Next client');
    //     this.totalList = dataClient;
    //   },(error) => {
    //     // Error
    //     console.error('CallObservableComponent error', error);
    //   }, () => {
    //     // Complete
    //     console.log('CallObservableComponent Complete client');
    //     this.isComplete = true;
    //   }
    // );
  }

  updateCommande() {
    console.log('Updating');
  }

  deleteCommande() {
    console.log('Deleting');
  }

  ngOnDestroy() {
    console.log('displaySelectComponent destroy! Boom !');
    if (this.subscriptionMessage$) {
      console.log('displaySelectComponent unsubscribe!');
      this.subscriptionMessage$.unsubscribe();
    }
  }
}
