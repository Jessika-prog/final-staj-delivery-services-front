import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DroneWebService } from '../shared/web-services/drone.webservice';
import { Drone } from '../shared/models/drone.model';

@Component({
  selector: 'app-gerer-flotte',
  templateUrl: './gerer-flotte.component.html',
  styleUrls: ['./gerer-flotte.component.scss']
})
export class GererFlotteComponent implements OnInit, OnDestroy {

  constructor(private droneWebService: DroneWebService) { }

  private subscriptionMessage$: Subscription;

  dronesList: Drone[];
  isComplete = false;

  ngOnInit() {
    this.subscriptionMessage$ = this.droneWebService.getDrones().subscribe(
      (data) => {
        // Next
        console.log('Affiche drone', data);
        this.dronesList = data;
      }, (error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }, () => {
        // Complete
        console.log('CallObservableComponent Complete');
        this.isComplete = true;
      }
    );
  }

  addDrone() {
    this.subscriptionMessage$ = this.droneWebService.addDrone().subscribe(
      () => {
        // Next
        console.log('Ajout du drone');
      }, (error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }, () => {
        // Complete
        console.log('CallObservableComponent Complete');
        this.isComplete = true;
      }
    );
    console.log(this.dronesList);
  }


  ngOnDestroy() {
    console.log('displaySelectComponent destroy! Boom !');
    if (this.subscriptionMessage$) {
      console.log('displaySelectComponent unsubscribe!');
      this.subscriptionMessage$.unsubscribe();
    }
  }

}
