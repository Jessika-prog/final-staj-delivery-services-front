import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GererFlotteComponent } from './gerer-flotte.component';

describe('GererFlotteComponent', () => {
  let component: GererFlotteComponent;
  let fixture: ComponentFixture<GererFlotteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GererFlotteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GererFlotteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
