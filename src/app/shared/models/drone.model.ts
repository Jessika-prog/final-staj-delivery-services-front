export class Drone {
    id: number;
    longitude: number;
    latitude: number;
    statut: string;

    constructor(
        idParam: number,
        longitudeParam: number,
        latitudeParam: number,
        statutParam: string)
        {
        this.id = idParam;
        this.longitude = longitudeParam;
        this.latitude = latitudeParam;
        this.statut = statutParam;
    }

}
