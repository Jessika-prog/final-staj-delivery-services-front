
export class Livraison {
    id: number;
    dateLivraison: string;
    commandeId: number;
    droneId: number;



    constructor(
        id: number,
        dateLivraison: string,
        commandeId: number,
        droneId: number

        )
        {
        this.id = id;
        this.dateLivraison = dateLivraison;
        this.commandeId = commandeId;
        this.droneId = droneId;
    }

}