import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';

import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

import { Livraison } from '../models/livraison.model';

@Injectable({
  providedIn: 'root'
})
export class LivraisonWebService {

  baseUrl = 'https://staj-delivery-services.herokuapp.com/';

  constructor(private http: HttpClient) { }

  getLivraisons(): Observable<Livraison[]> {
    return this.http.get<Livraison[]>(this.baseUrl + 'livraisons')
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  addDelivery(livraison): Observable<any> {
    return this.http.post(this.baseUrl + 'livraisons/livraison', null)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  // updateUser(user: User, userId: number): Observable<any> {
  //   return this.http.put(this.baseUrl + 'users/' + userId, user)
  //     .pipe(
  //       catchError((error) => this.handleError(error))
  //     );
  // }

//   deleteDrone(droneId: number): Observable<any> {
//     return this.http.delete(this.baseUrl + 'commande/' + commandeId)
//       .pipe(
//         catchError((error) => this.handleError(error))
//       );
//   }

  private handleError(error: HttpErrorResponse) {
    console.log('CommandeWebService error', error);

    return throwError('Something bad happened; please try again later.');
  }

}
