import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { Commande } from '../models/commande.model';

import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommandeWebService {

  baseUrl = 'https://staj-delivery-services.herokuapp.com/';

  constructor(private http: HttpClient) { }

  getCommandes(): Observable<Commande[]> {
    return this.http.get<Commande[]>(this.baseUrl + 'commandes')
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  addDrone(): Observable<any> {
    return this.http.post(this.baseUrl + 'commandes/commande', null)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  // updateUser(user: User, userId: number): Observable<any> {
  //   return this.http.put(this.baseUrl + 'users/' + userId, user)
  //     .pipe(
  //       catchError((error) => this.handleError(error))
  //     );
  // }

//   deleteDrone(droneId: number): Observable<any> {
//     return this.http.delete(this.baseUrl + 'commande/' + commandeId)
//       .pipe(
//         catchError((error) => this.handleError(error))
//       );
//   }

  private handleError(error: HttpErrorResponse) {
    console.log('CommandeWebService error', error);

    return throwError('Something bad happened; please try again later.');
  }

}
