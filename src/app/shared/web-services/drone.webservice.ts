import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Drone } from '../models/drone.model';

import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DroneWebService {

  baseUrl = 'https://staj-delivery-services.herokuapp.com/';

  constructor(private http: HttpClient) { }

  getDrones(): Observable<Drone[]> {
    return this.http.get<Drone[]>(this.baseUrl + 'drones')
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  addDrone(): Observable<any> {
    return this.http.post(this.baseUrl + 'drones/drone', null)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }
  findDrone(id: number): Observable<any> {
    return this.http.get<Drone>(this.baseUrl + 'drones/' + id)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.log('DroneWebService error', error);

    return throwError('Something bad happened; please try again later.');
  }

}
