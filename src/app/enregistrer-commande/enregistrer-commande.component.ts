import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Livraison } from '../shared/models/livraison.model';
import { LivraisonWebService } from '../shared/web-services/livraison.webservice';

@Component({
  selector: 'app-enregistrer-commande',
  templateUrl: './enregistrer-commande.component.html',
  styleUrls: ['./enregistrer-commande.component.scss']
})
export class EnregistrerCommandeComponent implements OnInit, OnDestroy {

  private subscriptionMessage$: Subscription;

  constructor( private livraisonWebService: LivraisonWebService) { }

  livraisonList: Livraison[];
  isComplete = false;
  livraison: Livraison;

  id: number;
  dateLivraison = '2020-02-21T12:18:34.183+0000';
  commandeId: number;
  droneId: number;


  ngOnInit() {
    this.subscriptionMessage$ = this.livraisonWebService.getLivraisons().subscribe(
      (data) => {
        // Next
        console.log('Affiche livraison', data);
        this.livraisonList = data;
      }, (error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }, () => {
        // Complete
        console.log('CallObservableComponent Complete');
        this.isComplete = true;
      }
    );
  }

  addDelivery() {
    console.log(this.livraison);

    this.livraison = new Livraison(this.id, this.dateLivraison, this.commandeId, this.droneId);

    this.subscriptionMessage$ = this.livraisonWebService.addDelivery(this.livraison).subscribe(
      () => {
        // Next
        console.log('ça ajoute');

      }, (error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }, () => {
        // Complete
        console.log('CallObservableComponent Complete');
        this.isComplete = true;
      }
    );
  }

  ngOnDestroy() {
    console.log('displaySelectComponent destroy! Boom !');
    if (this.subscriptionMessage$) {
      console.log('displaySelectComponent unsubscribe!');
      this.subscriptionMessage$.unsubscribe();
    }
  }
}
